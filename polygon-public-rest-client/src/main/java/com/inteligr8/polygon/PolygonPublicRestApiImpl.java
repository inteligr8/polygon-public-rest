package com.inteligr8.polygon;

import com.inteligr8.polygon.api.StocksApiV1;
import com.inteligr8.rs.Client;

/**
 * This class serves as the entrypoint to the JAX-RS API for the Polygon.IO
 * Public ReST API.
 * 
 * @author brian@inteligr8.com
 */
public class PolygonPublicRestApiImpl implements PolygonPublicRestApi {
	
	private final Client client;
	
	public PolygonPublicRestApiImpl(Client client) {
		this.client = client;
	}
	
	protected final <T> T getApi(Class<T> apiClass) {
		return this.client.getApi(apiClass);
	}
	
	public StocksApiV1 getStocksApi() {
		return this.getApi(StocksApiV1.class);
	}

}
