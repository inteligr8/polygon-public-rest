package com.inteligr8.polygon;

import java.io.IOException;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.UriBuilder;

import com.inteligr8.rs.AuthorizationFilter;

public class PolygonAuthorizationFilter implements AuthorizationFilter {
	
	private String apiKey;
	
	public PolygonAuthorizationFilter(String apiKey) {
		this.apiKey = apiKey;
	}
	
	@Override
	public void filter(ClientRequestContext requestContext) throws IOException {
		this.authWithHeader(requestContext);
	}
	
	protected void authWithHeader(ClientRequestContext requestContext) {
		requestContext.getHeaders().add(HttpHeaders.AUTHORIZATION, "Bearer " + this.apiKey);
	}
	
	protected void authWithQueryParam(ClientRequestContext requestContext) {
		requestContext.setUri(
				UriBuilder.fromUri(requestContext.getUri())
					.queryParam("apiKey", this.apiKey)
					.build());
	}

}
