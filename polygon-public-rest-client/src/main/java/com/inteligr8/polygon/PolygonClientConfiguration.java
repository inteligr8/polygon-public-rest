package com.inteligr8.polygon;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.inteligr8.rs.AuthorizationFilter;
import com.inteligr8.rs.ClientCxfConfiguration;
import com.inteligr8.rs.ClientJerseyConfiguration;

/**
 * This class provides a POJO &amp; Spring-based implementation of the
 * ClientConfiguration interface.  You can use it outside of the Spring
 * context, but you will need the spring-context and spring-beans libraries in
 * your non-Spring application.
 * 
 * @author brian@inteligr8.com
 */
@Configuration
@ComponentScan
public class PolygonClientConfiguration implements ClientCxfConfiguration, ClientJerseyConfiguration {

	@Value("${polygon.service.baseUrl:https://api.polygon.io}")
	private String baseUrl;

	@Value("${polygon.service.security.auth.apiKey}")
	private String apiKey;
	
	@Value("${polygon.service.cxf.defaultBusEnabled:true}")
	private boolean defaultBusEnabled;
	
	@Value("${polygon.service.jersey.putBodyRequired:true}")
	private boolean putBodyRequired;

	public String getBaseUrl() {
		return this.baseUrl;
	}
	
	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}
	
	public String getApiKey() {
		return this.apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public boolean isDefaultBusEnabled() {
		return this.defaultBusEnabled;
	}

	public void setDefaultBusEnabled(boolean defaultBusEnabled) {
		this.defaultBusEnabled = defaultBusEnabled;
	}
	
	public boolean isPutBodyRequired() {
		return this.putBodyRequired;
	}

	public void setPutBodyRequired(boolean putBodyRequired) {
		this.putBodyRequired = putBodyRequired;
	}
	
	
	
	@Override
	public AuthorizationFilter createAuthorizationFilter() {
		return new PolygonAuthorizationFilter(this.getApiKey());
	}

}
