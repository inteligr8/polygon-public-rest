package com.inteligr8.polygon;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import com.inteligr8.rs.ClientJerseyConfiguration;
import com.inteligr8.rs.ClientJerseyImpl;

/**
 * This class provides a POJO &amp; Spring-based implementation of the Jersey
 * client.  You can use it outside of the Spring context, but you will need the
 * spring-context and spring-beans libraries in your non-Spring application.
 * 
 * @author brian@inteligr8.com
 */
@Component("polygon.client")
@Lazy
public class PolygonClientJerseyImpl extends ClientJerseyImpl {
	
	@Autowired
	private PolygonClientConfiguration config;
	
	private final PolygonPublicRestApi api;
	
	/**
	 * This constructor is for Spring use.
	 */
	protected PolygonClientJerseyImpl() {
		this.api = new PolygonPublicRestApiImpl(this);
	}
	
	/**
	 * This constructor is for POJO use.
	 * @param config
	 */
	public PolygonClientJerseyImpl(PolygonClientConfiguration config) {
		this.config = config;
		this.api = new PolygonPublicRestApiImpl(this);
	}
	
	@Override
	protected ClientJerseyConfiguration getConfig() {
		return this.config;
	}
	
	public PolygonPublicRestApi getApi() {
		return this.api;
	}

}
