package com.inteligr8.polygon;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import com.inteligr8.rs.ClientCxfConfiguration;
import com.inteligr8.rs.ClientCxfImpl;

/**
 * This class provides a POJO &amp; Spring-based implementation of the Apache
 * CXF client.  You can use it outside of the Spring context, but you will need
 * the spring-context and spring-beans libraries in your non-Spring
 * application.
 * 
 * @author brian@inteligr8.com
 */
@Component("polygon.client")
@Lazy
public class PolygonClientCxfImpl extends ClientCxfImpl {
	
	@Autowired
	private PolygonClientConfiguration config;
	
	private final PolygonPublicRestApi api;

	/**
	 * This constructor is for Spring use.
	 */
	protected PolygonClientCxfImpl() {
		this.api = new PolygonPublicRestApiImpl(this);
	}
	
	/**
	 * This constructor is for POJO use.
	 * @param config
	 */
	public PolygonClientCxfImpl(PolygonClientConfiguration config) {
		this.config = config;
		this.api = new PolygonPublicRestApiImpl(this);
	}
	
	@Override
	protected ClientCxfConfiguration getConfig() {
		return this.config;
	}

	public PolygonPublicRestApi getApi() {
		return this.api;
	}

}
