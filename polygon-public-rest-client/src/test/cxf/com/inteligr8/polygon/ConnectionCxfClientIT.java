package com.inteligr8.polygon;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import com.inteligr8.rs.ClientConfiguration;

@TestPropertySource(locations = {"/polygon-personal.properties"})
@SpringJUnitConfig(classes = {PolygonClientConfiguration.class, PolygonClientCxfImpl.class})
public class ConnectionCxfClientIT extends ConnectionClientIT {
	
	@Autowired
	private PolygonClientCxfImpl client;
	
	@Override
	public PolygonPublicRestApi getClient() {
		return this.client.getApi();
	}
	
	@Override
	public ClientConfiguration getConfiguration() {
		return this.client.getConfig();
	}
	
}
