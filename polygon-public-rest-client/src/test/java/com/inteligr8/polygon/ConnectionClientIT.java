package com.inteligr8.polygon;

import java.time.LocalDate;

import javax.ws.rs.BadRequestException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.EnabledIf;
import org.junit.jupiter.api.function.Executable;

import com.inteligr8.polygon.api.StocksApiV1;
import com.inteligr8.polygon.model.StockDateSummary;

public abstract class ConnectionClientIT extends ConditionalIT {
	
	public abstract PolygonPublicRestApi getClient();
	
	private StocksApiV1 api;
	
	public boolean fullTest() {
		return false;
		//return this.hostExists();
	}
	
	@BeforeEach
	public void getApi() {
		this.api = this.getClient().getStocksApi();
	}
	
	@Test
	@EnabledIf("fullTest")
	public void testValidTickerToday() {
		Assertions.assertThrows(BadRequestException.class, new Executable() {
			@Override
			public void execute() {
				api.getStockSummaryOnDate("AAPL", LocalDate.now());
			}
		});
	}
	
	@Test
	@EnabledIf("fullText")
	public void testValidTicketTomorrow() {
		Assertions.assertThrows(BadRequestException.class, new Executable() {
			@Override
			public void execute() {
				api.getStockSummaryOnDate("AAPL", LocalDate.now().plusDays(1L));
			}
		});
	}
	
	@Test
	@EnabledIf("fullTest")
	public void testValidTickerYesterday() {
		StockDateSummary summary = this.api.getStockSummaryOnDate("AAPL", LocalDate.now().minusDays(1L));
		this.assertStockDateSummary(summary);
	}
	
	@Test
	@EnabledIf("fullTest")
	public void testInvalidTicker() {
		Assertions.assertThrows(BadRequestException.class, new Executable() {
			@Override
			public void execute() {
				api.getStockSummaryOnDate("NOT-A-TICKER", LocalDate.now().plusDays(1L));
			}
		});
	}
	
	private void assertStockDateSummary(StockDateSummary summary) {
		Assertions.assertNotNull(summary);
		Assertions.assertEquals("AAPL", summary.getSymbol());
		Assertions.assertNotNull(summary.getOpen());
		Assertions.assertNotNull(summary.getHigh());
		Assertions.assertNotNull(summary.getLow());
		Assertions.assertNotNull(summary.getClose());
		Assertions.assertTrue(summary.getLow() <= summary.getHigh());
		Assertions.assertTrue(summary.getLow() <= summary.getClose());
		Assertions.assertTrue(summary.getLow() <= summary.getOpen());
		Assertions.assertTrue(summary.getClose() <= summary.getHigh());
		Assertions.assertTrue(summary.getOpen() <= summary.getHigh());
	}
	
}
