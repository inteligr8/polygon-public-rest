package com.inteligr8.polygon;

import javax.ws.rs.core.Response.Status;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.impl.client.DefaultRedirectStrategy;
import org.apache.http.impl.client.HttpClientBuilder;

import com.inteligr8.rs.ClientConfiguration;

public abstract class ConditionalIT {
	
	public abstract ClientConfiguration getConfiguration();
	
	public boolean hostExists() {
		String uri = this.getConfiguration().getBaseUrl();
		
		HttpUriRequest request = RequestBuilder.get()
				.setUri(uri)
				.build();
		
		HttpClient client = HttpClientBuilder.create()
				.setRedirectStrategy(DefaultRedirectStrategy.INSTANCE)
				.build();
		
		try {
			HttpResponse response = client.execute(request);
			return response.getStatusLine().getStatusCode() < 300 ||
					response.getStatusLine().getStatusCode() == Status.NOT_FOUND.getStatusCode();
		} catch (Exception e) {
			return false;
		}
	}

}
