package com.inteligr8.polygon;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import com.inteligr8.rs.ClientConfiguration;

@TestPropertySource(locations = {"/polygon-personal.properties"})
@SpringJUnitConfig(classes = {PolygonClientConfiguration.class, PolygonClientJerseyImpl.class})
public class ConnectionJerseyClientIT extends ConnectionClientIT {
	
	@Autowired
	private PolygonClientJerseyImpl client;
	
	@Override
	public PolygonPublicRestApi getClient() {
		return this.client.getApi();
	}
	
	@Override
	public ClientConfiguration getConfiguration() {
		return this.client.getConfig();
	}
	
}
