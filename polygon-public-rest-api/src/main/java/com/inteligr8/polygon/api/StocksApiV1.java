package com.inteligr8.polygon.api;

import java.time.LocalDate;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.inteligr8.polygon.model.StockDateSummary;

@Path("/v1")
public interface StocksApiV1 {

    @GET
    @Path("/open-close/{stocksTicker}/{date}")
    @Produces({ MediaType.APPLICATION_JSON })
    public StockDateSummary getStockSummaryOnDate(
    		@PathParam("stocksTicker") String stockTicker,
    		@PathParam("date") @JsonFormat(pattern = "yyyy-MM-dd") LocalDate date);

    @GET
    @Path("/open-close/{stocksTicker}/{date}")
    @Produces({ MediaType.APPLICATION_JSON })
    public StockDateSummary getStockSummaryOnDate(
    		@PathParam("stocksTicker") String stockTicker,
    		@PathParam("date") @JsonFormat(pattern = "yyyy-MM-dd") LocalDate date,
    		@QueryParam("adjusted") boolean adjusted);

}
