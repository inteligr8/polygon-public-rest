package com.inteligr8.polygon.model;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author brian@inteligr8.com
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class StockDateSummary extends BaseResponse {

	@JsonProperty
	private String symbol;

	@JsonProperty
	private Double preMarket;

	@JsonProperty
	private Double open;

	@JsonProperty
	private Double high;

	@JsonProperty
	private Double low;

	@JsonProperty
	private Double close;

	@JsonProperty
	private Long volume;

	@JsonProperty
	private Double afterHours;
	
	@JsonProperty
	@JsonFormat(pattern = "yyyy-MM-dd")
	private LocalDate from;
	


	public String getSymbol() {
		return this.symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public Double getPreMarket() {
		return this.preMarket;
	}

	public void setPreMarket(Double preMarket) {
		this.preMarket = preMarket;
	}

	public Double getOpen() {
		return this.open;
	}

	public void setOpen(Double open) {
		this.open = open;
	}

	public Double getHigh() {
		return this.high;
	}

	public void setHigh(Double high) {
		this.high = high;
	}

	public Double getLow() {
		return this.low;
	}

	public void setLow(Double low) {
		this.low = low;
	}

	public Double getClose() {
		return this.close;
	}

	public void setClose(Double close) {
		this.close = close;
	}

	public Long getVolume() {
		return this.volume;
	}

	public void setVolume(Long volume) {
		this.volume = volume;
	}

	public Double getAfterHours() {
		return this.afterHours;
	}

	public void setAfterHours(Double afterHours) {
		this.afterHours = afterHours;
	}

	public LocalDate getFrom() {
		return this.from;
	}

	public void setFrom(LocalDate from) {
		this.from = from;
	}

}
