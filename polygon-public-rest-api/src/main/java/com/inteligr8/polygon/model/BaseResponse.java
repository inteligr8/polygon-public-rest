package com.inteligr8.polygon.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BaseResponse {
	
	public enum Status {
		@JsonProperty("OK")
		Ok
	}
	
	@JsonProperty
	private Status status;
	
	
	
	public Status getStatus() {
		return this.status;
	}
	
	public void setStatus(Status status) {
		this.status = status;
	}

}
