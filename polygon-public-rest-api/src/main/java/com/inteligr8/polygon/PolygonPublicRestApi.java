package com.inteligr8.polygon;

import com.inteligr8.polygon.api.StocksApiV1;

/**
 * This interface consolidates the JAX-RS APIs available in the Polygon.IO
 * Public ReST API.
 * 
 * @author brian@inteligr8.com
 */
public interface PolygonPublicRestApi {
	
	StocksApiV1 getStocksApi();

}
